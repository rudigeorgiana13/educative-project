import React from 'react'
import classes from '../../Style/Home-Main.module.css'
import ButtonOrangeThikShort from "../../../UI/buttons/OrangeButtons/ButtonOrangeThikShort";
import superMarioImg from '../../../UI/img/super-mario.png'
import playStationImg from '../../../UI/img/playstation.png'
import barImg from '../../../UI/img/bar.png'


const MainSecondPart = () => {

  return (
    <div className={classes.mainSecondPartContainer}>
      <div className={classes.titleMainSecondPart}> Our Courses </div>

      <div className={classes.coursesContainer}>
        <div className={classes.courseContainer}>
            <div>
              <img src={superMarioImg} className={classes.imageMainSecondPart} />
            </div>
            <div className={classes.courseContainerText}>
              <div className={classes.titleCourse}> Game Design Essentials</div>
              <div> 8hrs</div>
            </div>
          <div className={classes.bar}> <img src={barImg}/> </div>
        </div>

        <div className={classes.courseContainer}>
              <div> <img src={playStationImg} className={classes.imageMainSecondPart} /> </div>
              <div className={classes.courseContainerText}>
                <div className={classes.titleCourse}> Unity Game Engine <br/> Fundamentals</div>
                <div> 8hrs </div>
              </div>
          <div className={classes.bar}> <img src={barImg}/> </div>
        </div>
      </div>


      <div className={classes.buttonSecondPart}> <ButtonOrangeThikShort text={'All Courses'}/> </div>
    </div>

  )
}

export default MainSecondPart