import React from 'react'
import classes from "../../Style/Home-Main.module.css";
import rectangle from "../../../UI/img/Rectangle-Cards-Container.png";
import janeImg from "../../../UI/img/Jane-Cooper.png";
import jacobImg from "../../../UI/img/Jacob-Jones.png";
import ButtonOrangeThikShort from "../../../UI/buttons/OrangeButtons/ButtonOrangeThikShort";


const MainFirstPart = () => {
  return (
    <div className={classes.cardsContainer}>

      <div className={classes.peopleCardsContainer}>
        <img src={rectangle} className={classes.rectangle}/>
        <div className={classes.firstCard}>
          <div className={classes.titleCard}>
            <p className={classes.p}> This is a great course.It helped me a lot. <br/> Thank you :)</p>
          </div>
          <div className={classes.peopleInfo}>

            <div className={classes.fullNameAndJobAndPhoto}>
              <div className={classes.nameAndJob}>
                <div className={classes.name}> Jane Cooper</div>
                <div className={classes.job}> Developer, Sony</div>
              </div>
              <div className={classes.photoFirstCard}><img src={janeImg}/></div>
            </div>

          </div>
        </div>

        <div className={classes.secondCard}>
          <div className={classes.secondTitleCard}>
            <p className={classes.p}> Amazing work! Well Done! </p>
          </div>
          <div className={classes.peopleInfoSecondCard}>

            <div className={classes.fullNameAndJobAndPhoto}>
              <div className={classes.nameAndJobSecondCard}>
                <div className={classes.name}> Jacob Jones</div>
                <div className={classes.job}> Designer, Facebook</div>
              </div>
              <div className={classes.photoSecondCard}><img src={jacobImg}/></div>
            </div>

          </div>
        </div>
      </div>
      <div className={classes.textContainer}>
        <div className={classes.title}> What our students <br/> say</div>
        <div className={classes.description}>
          All students get access to all the videos and also the source
          code of <br/> the projects. <br/> You will also have access to private
          Discord channel where you can <br/> discuss your doubts.
        </div>
        <div className={classes.textContainerBtn}><ButtonOrangeThikShort text={'Learn More'}/></div>
      </div>
    </div>
  )
}

export default MainFirstPart