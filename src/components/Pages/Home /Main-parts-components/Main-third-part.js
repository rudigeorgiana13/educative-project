import React from 'react'
import classes from '../../Style/Home-Main.module.css'
import img1 from '../../../UI/icons/img/googlePlay/Vector (2).png'
import img2 from '../../../UI/icons/img/googlePlay/Vector (3).png'
import img3 from '../../../UI/icons/img/googlePlay/Vector (4).png'
import img4 from '../../../UI/icons/img/googlePlay/Vector (5).png'

import appStore from '../../../UI/icons/img/app-Store.png'


const MainThirdPart = () => {
  return (
    <div className={classes.appContainer}>

      <div className={classes.titleApp}> Get our App </div>
      <div className={classes.descriptionApp}> You can use our App for better experience on smartphones </div>

      <div className={classes.showAppContainer}>
        <div className={classes.iphoneApp}>
          <div className={classes.appStoreIcon}> <img src={appStore} />  </div>
          <div className={classes.titleAppStore}>  App store </div>
        </div>
        <div className={classes.androidApp}>
            <div> <img src={img1}  className={classes.img1} />  </div>
            <div> <img src={img2}  className={classes.img2}/>  </div>
            <div> <img src={img3}  className={classes.img3}/>  </div>
            <div> <img src={img4}  className={classes.img4}/>  </div>
          <div  className={classes.titleGooglePlay}> Google play </div>
        </div>
      </div>

    </div>
  )
}

export default MainThirdPart