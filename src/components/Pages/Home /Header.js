import React from 'react'
import classes from "../Style/Home-Header.module.css";
import {Link} from "react-router-dom";
import ButtonOrangeThikLong from "../../UI/buttons/OrangeButtons/ButtonOrangeThikLong";
import image from '../../UI/img/hero-image 1.png'

import {IoMdStar} from 'react-icons/io'

const Header = () => {

  return (
    <header className={classes.headerContainer}>
      <div className={classes.navContainer}>
        <div className={classes.abrev}> GD.</div>
        <div className={classes.links}>
            <Link to="/about" className={classes.about}> About </Link>
            <Link to="/services" className={classes.services}> Services </Link>
            <Link to="/our-work" className={classes.ourWork}> Our Work </Link>
            <button className={classes.enrollNowBtn}>
              <Link to="/enrol-now" className={classes.enrollNowText}> Enrol Now </Link>
            </button>
        </div>
      </div>

      <div>
        <div className={classes.textContainer}>
            <h1> Learn the art of
              <br/> Game Dev
            </h1>

          <p>
            This is a comprehensive course on Game Development. <br/>
            You will learn everything from generating an idea to <br/>
            publishing your games to different platforms
          </p>
          <div className={classes.orangeButton}>
            <ButtonOrangeThikLong text={'Enroll now'}/>
          </div>
        </div>
      </div>
      <div className={classes.img}>
        <img src={image}/>
      </div>


      <div className={classes.ticket1}>
          <h1> 32K </h1>
          <p> Students Enrolled</p>
      </div>
        <div className={classes.ticket2}>
          <div className={classes.starNumber}>
          <div className={classes.number}> 4.7</div>
          <div className={classes.star}><IoMdStar/></div>
          </div>

        <p> Overall Rating</p>
      </div>
    </header>

  )
}

export default Header