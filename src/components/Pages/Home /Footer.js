import React from 'react'
import classes from '../Style/Home-Footer.module.css'

import {BsInstagram} from 'react-icons/bs'
import {BsFacebook} from 'react-icons/bs'


const Footer = () => {
    return (
      <div className={classes.footerContainer}>

        <div className={classes.footerLinksContainer}>
          <div className={classes.quickLinks}>
            <div> Quick Links </div>
            <div> About </div>
            <div> Contact Us </div>
            <div> Privacy Policy </div>
            <div> Terms & Conditions </div>
          </div>
          <div className={classes.course}>
            <div> Course </div>
            <div>Log In</div>
            <div>Download</div>
            <div>All Courses</div>
          </div>
          <div className={classes.contactUs}>
            <div className={classes.contactUsHeader}> Contact Us </div>
            <div className={classes.emailFooter}>contact@email.com</div>
            <div className={classes.iconsWrapper}>
               <BsFacebook  className={classes.fb} />
               <BsInstagram className={classes.in} />
            </div>
            <div className={classes.inputButtonWrapper}>
                <input className={classes.input} placeholder={'Email Address'} />
              <button className={classes.button}> <p> Subscribe </p> </button>
            </div>
          </div>
        </div>


        <div className={classes.textFooter}>
          <p> This website is developed by GTCoding © 2021 </p>
        </div>

      </div>
    )
}

export default Footer