import React from 'react'
import classes from '../Style/Home-Main.module.css'
import MainFirstPart from "./Main-parts-components/Main-first-part";
import MainSecondPart from "./Main-parts-components/Main-second-part";
import MainThirdPart from "./Main-parts-components/Main-third-part";


const Main = () => {
return (
  <div className={classes.mainContainer}>
      <MainFirstPart />
      <MainSecondPart />
      <MainThirdPart />
  </div>
)
}


export default Main