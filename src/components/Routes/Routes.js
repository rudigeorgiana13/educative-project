import { Routes, Route, Link } from "react-router-dom";

import Home from "../Pages/Home /Home"
import About from "../Pages/About";
import Services from "../Pages/Services";
import OurWork from "../Pages/OurWork";
import EnrolNow from "../Pages/EnrolNow";


const Routing = () =>  {
  return (
    <>
      <Routes>
        <Route path='/' element={<Home/>} />
        <Route path='/about' element={<About/>} />
        <Route path='/services' element={<Services/>} />
        <Route path='/our-work' element={<OurWork/>} />
        <Route path='/enrol-now' element={<EnrolNow/>} />
      </Routes>
    </>
  );
}

export default Routing