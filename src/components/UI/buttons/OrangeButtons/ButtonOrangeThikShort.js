import React from 'react'
import classes from './ButtonOrange.module.css'


const ButtonOrangeThikShort = (props) => {
  return (
    <div>
      <button className={classes.buttonOrangeThikShort}>  {props.text} </button>
    </div>
  )
}


export default ButtonOrangeThikShort