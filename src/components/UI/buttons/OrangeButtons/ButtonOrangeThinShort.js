import React from "react";
import classes from "./ButtonOrange.module.css"


const ButtonOrangeThinShort = (props) => {
 return (
   <div>
     <button className={classes.buttonOrangeThinShort}> {props.text} </button>
   </div>
 )
}

export default ButtonOrangeThinShort