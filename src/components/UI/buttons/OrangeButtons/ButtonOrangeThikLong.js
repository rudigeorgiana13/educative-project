import React from 'react'
import classes from './ButtonOrange.module.css'
import {FiEdit} from 'react-icons/fi'


const ButtonOrangeThikLong = (props) => {
  return (
    <div>
      <button className={classes.buttonOrangeThikLong}> <FiEdit /> {props.text} </button>
    </div>
  )
}


export default ButtonOrangeThikLong