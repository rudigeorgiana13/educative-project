import React from 'react'
import classes from './ButtonOrange.module.css'
import {FiEdit} from 'react-icons/fi'



const ButtonOrangeThinLong = (props) => {
  return (
    <div>
    <button className={classes.buttonOrangeThinLong}> <FiEdit /> {props.text}</button>
    </div>
  )
}

export default ButtonOrangeThinLong