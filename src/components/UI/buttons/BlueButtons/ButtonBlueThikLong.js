import React from 'react'
import classes from '../OrangeButtons/ButtonOrange.module.css'
import {FiEdit} from 'react-icons/fi'


const ButtonBlueThikLong = (props) => {
  return (
    <div>
      <button className={classes.buttonBlueThikLong}> <FiEdit /> {props.text} </button>
    </div>
  )
}


export default ButtonBlueThikLong