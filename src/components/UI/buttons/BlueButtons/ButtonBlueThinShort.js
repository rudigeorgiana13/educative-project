import React from 'react'
import classes from '../OrangeButtons/ButtonOrange.module.css'



const ButtonBlueThinShort = (props) => {
  return (
    <div>
      <button className={classes.buttonBlueThinShort}> {props.text} </button>
    </div>
  )
}


export default ButtonBlueThinShort