import React from 'react'
import classes from '../OrangeButtons/ButtonOrange.module.css'
import {FiEdit} from 'react-icons/fi'


const ButtonBlueThinLong = (props) => {
  return (
    <div>
      <button className={classes.buttonBlueThinLong}> <FiEdit /> {props.text} </button>
    </div>
  )
}


export default ButtonBlueThinLong