import React from 'react'
import Routes from "./components/Routes/Routes";
import classes from './App.module.css'
import Home from '../src/components/Pages/Home /Home'


const App = () =>  {
  return (
    <div className="App">
    <Routes />
    <Home/>
    </div>
  );
}

export default App;
